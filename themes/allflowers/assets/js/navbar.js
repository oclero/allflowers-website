'use strict';

document.addEventListener('DOMContentLoaded', function () {
  // Prevent scroll on the page when the mobile menu is visible.
  const NO_SCROLL_CSS_CLASS = 'noScroll';
  const MOBILE_NAVBAR_CSS_CLASS = 'mobile';

  const menuSections = document.querySelectorAll('a.nav-item');
  const menuCheckbox = document.getElementById('menu-checkbox');
  const bodyElement = document.getElementsByTagName('body')[0];
  const navBar = document.getElementById('navbar');

  if (!navBar) return;
  if (!menuCheckbox) return;

  function showMenu(show) {
    if (show) {
      bodyElement.classList.add(NO_SCROLL_CSS_CLASS);
      navBar.classList.add(MOBILE_NAVBAR_CSS_CLASS);
      menuCheckbox.checked = true;
    } else {
      bodyElement.classList.remove(NO_SCROLL_CSS_CLASS);
      navBar.classList.remove(MOBILE_NAVBAR_CSS_CLASS);
      menuCheckbox.checked = false;
    }
  }

  showMenu(false);

  window.addEventListener('load', function () {
    menuCheckbox.addEventListener('change', (event) => {
      showMenu(event.target.checked);
    });
  });

  // Mobile-only: Close the navbar menu after item has been clicked.
  menuSections.forEach((menuSection) => {
    menuSection.addEventListener('click', function () {
      showMenu(false);
    });
  });

  bodyElement.addEventListener('keydown', (event) => {
    if (event.key === 'Escape') {
      showMenu(false);
    }
  });
});
