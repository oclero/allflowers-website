<div align="center" style="margin-bottom: 1em;">
  <img alt="logo" src="themes/allflowers/assets/img/logo.svg" width="300px"/>
</div>

# Allflowers Website

[![License: MIT](https://img.shields.io/badge/License-MIT-lightgrey.svg)](https://creativecommons.org/licenses/by-sa/4.0/)
[![Pipeline Status](https://gitlab.com/allflowers/allflowers-website/badges/main/pipeline.svg)](https://gitlab.com/allflowers/allflowers-website/commits/main)

Website source code for Allflowers, an Alternative Rock band from Paris, France.

## 📦 Setup

1. First, install `hugo`. Be sure to use the `extended` version that has SCSS support.

    - Ubuntu:

        ```bash
        snap install hugo --channel=extended
        ```
    - MacOS:

        ```bash
        brew install hugo
        ```

    - Windows:

        ```bash
        choco install hugo-extended
        ```

2. To run the website locally, go to the website directory, then type:

    ```bash
    hugo serve
    ```

## 🛠️ Build

To build the website to the `/public` directory, ready to be deployed:

```bash
rm -rf ./public
hugo --minify
```

## ⚖️ License

The source code is licensed under the terms of the [MIT License](https://opensource.org/licenses/MIT).

Content itself is copyrighted.

Pictures and logos are copyrighted (Olivier Cléro, Emilie Vernerey, Judith Wahler).
