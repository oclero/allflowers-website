---
title: Technical Docs
aliases:
  - /technical-docs
---

Here are **Technical Riders** for our shows (information about rider, backline, etc.).

| Band Configuration | 🇫🇷 Français                                                    | 🇬🇧 English |
| :----------------- | -------------------------------------------------------------- | ---------- |
| Acoustic Duo       | [Fiche technique]({{< param "technicalRiderAcousticDuoFr" >}}) | _Soon_     |
| Electric Duo       | [Fiche technique]({{< param "technicalRiderElectricDuoFr" >}}) | _Soon_     |
| Full Band          | [Fiche technique]({{< param "technicalRiderFr" >}})            | _Soon_     |
