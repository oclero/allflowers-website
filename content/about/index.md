---
title: About
---

“All flowers in time bend towards the sun.”

Having chosen the title of this obscure gem from the 90s by [Jeff Buckley](https://www.youtube.com/watch?v=A3adFWKE9JE) and [Liz Fraser of Cocteau Twins](https://www.youtube.com/watch?v=6KnYw4EwYGc) as their band name, **Lara Martinovic** (vocals, keyboard) and **Olivier Cléro** (guitar, bass, drums) lay the foundations of their universe tinged with the spleen of the 90s.

Writing is a way for Lara to share this feeling of bewilderment and loss of meaning that an entire generation faces. Her haunting voice is carried by Olivier's arrangements and fierce guitars, which bring the duo to horizons evoking the golden age of UK and US Alternative Rock, from [the Cranberries](https://www.youtube.com/watch?v=Yam5uK6e-bQ) to [Pixies](https://www.youtube.com/watch?v=6VG6gIvcjU8), thus bridging the anxieties of our time and the existential questions of the grunge years.

{{< button text="Press access" url="/press" >}}

{{< photo-gallery "photos/allflowers-9.jpg" "photos/allflowers-21.jpg" "photos/allflowers-34.jpg" >}}
