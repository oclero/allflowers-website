---
title: Legal Terms
---

## Préambule

En vigueur au 06/02/2023.

Conformément aux dispositions des Articles 6-III et 19 de la Loi n°2004-575 du 21 juin 2004 pour la Confiance dans l’économie numérique, dite L.C.E.N., il est porté à la connaissance des utilisateurs et visiteurs (ci-après l'_Utilisateur_) du site [www.allflowers-music.com](https://www.allflowers-music.com) (ci-après le _Site_) les présentes mentions légales.

La connexion et la navigation sur le _Site_ par l’_Utilisateur_ implique acceptation intégrale et sans réserve des présentes mentions légales.

## Article 1 - L'éditeur

L’édition et la direction de la publication du _Site_ est assurée par **Olivier Cléro**, domicilié au **8 rue de Fécamp 75012 Paris, France**, dont le numéro de téléphone est [+33686669940](tel:+33686669940), et l'adresse e-mail oclero@protonmail.com. Ci-après l'_Éditeur_.

## Article 2 - L'hébergeur

L'hébergeur du _Site_ est la société **GitLab**, dont le siège social est situé au **268 Bush Street Suite 350, San Francisco, California, 94104, United States**, et dont le numéro de téléphone est [+14158292854](tel:+14158292854).

## Article 3 - Accès au _Site_

Le _Site_ est accessible en tout endroit, 7j/7, 24h/24, sauf cas de force majeure, interruption programmée ou non et pouvant découlant d’une nécessité de maintenance. En cas de modification, interruption ou suspension du Site, l'_Éditeur_ ne saurait être tenu responsable.

## Article 4 - Collecte des données

Le _Site_ est exempté de déclaration à la Commission Nationale Informatique et Libertés (CNIL) dans la mesure où il ne collecte aucune donnée concernant les utilisateurs. Le _Site_ n'utilise pas non plus de cookies.

Les articles de ce site peuvent cependant inclure des contenus intégrés (par exemple des vidéos, images, articles…). Le contenu intégré depuis d’autres sites se comporte de la même manière que si le visiteur se rendait sur cet autre site. Ces sites web pourraient collecter des données sur vous, utiliser des cookies, embarquer des outils de suivis tiers, suivre vos interactions avec ces contenus embarqués si vous disposez d’un compte connecté sur leur site web.

## Article 5 - Copyright

Toute utilisation, reproduction, diffusion, commercialisation, modification de toute ou partie du Site, sans autorisation de l’Éditeur, est prohibée et pourra entraîner des actions et poursuites judiciaires telles que notamment prévues par le Code de la propriété intellectuelle et le Code civil.

## Article 6 - Respect du droit d'auteur

Le _Site_ respecte le droit d’auteur. Tous les droits des auteurs des œuvres protégées, reproduites et communiquées sur ce site, sont réservés. Sauf autorisation expresse, toute utilisation des œuvres autre que la consultation dans le cadre du cercle de famille est strictement interdite.

{{< sacem-logo >}}
