---
title: Allflowers présentent le clip de leur nouveau single "Zagreb Song"
date: 2023-10-28
---

Le duo de rock alternatif **Allflowers** rend hommage à la capitale de la Croatie dans le clip de leur nouveau single, _Zagreb Song_.

La chanteuse du groupe, Lara Martinovic, est d’origine croate. Ses parents, tous deux croates, se sont installés à Paris au début des années 80. Chaque été, la famille avait pour habitude de se rendre à Zagreb en autocar, depuis Paris.

Le clip retrace ce voyage régulier qui a marqué l’enfance de Lara, et comporte exclusivement des séquences tirées de VHS filmées par sa mère sur la période 1994-1996. Il est empreint de nostalgie et sert élégamment l’émotion de la chanson, soutenue par les cordes jouées par Catherine Jacob et Barbara Rialland, et le clavier électrique de Louis Couka, invités à l’occasion.

Le single _Zagreb Song_ sort sur les plateformes de streaming le 28 octobre 2023 chez Verveine Records.

## Liens

- [Clip de Zagreb Song sur YouTube](https://www.youtube.com/watch?v=w2PK0eooleM)
- [Kit presse d'Allflowers]({{< param "pressKitFr" >}})

## Photos

Cliquez sur les photos pour une version en haute résolution.

{{< image "zagreb-song-thumbnail.jpg" "" "Photo de couverture" "yes" "medium" "Vera Zahorodni, Olivier Cléro" >}}

{{< image "zagreb-song-single.jpg" "" "Pochette du single Zagreb Song" "yes" "medium" >}}

{{< image "zagreb-song-snapshot-1.jpg" "" "Capture d'écran de la vidéo (1)" "yes" "medium" "Vera Zahorodni, Olivier Cléro" >}}

{{< image "zagreb-song-snapshot-2.jpg" "" "Capture d'écran de la vidéo (2)" "yes" "medium" "Vera Zahorodni, Olivier Cléro" >}}

{{< image "zagreb-song-snapshot-3.jpg" "" "Capture d'écran de la vidéo (3)" "yes" "medium" "Vera Zahorodni, Olivier Cléro" >}}

{{< image "zagreb-song-snapshot-4.jpg" "" "Capture d'écran de la vidéo (4)" "yes" "medium" "Vera Zahorodni, Olivier Cléro" >}}

{{< image "zagreb-song-snapshot-5.jpg" "" "Capture d'écran de la vidéo (5)" "yes" "medium" "Vera Zahorodni, Olivier Cléro" >}}
