---
title: Allflowers présentent le clip de leur premier single "Somewhere Between The Lines"
date: 2022-05-06
---

Le duo de rock alternatif **Allflowers** publie aujourd'hui le clip vidéo pour _Somewhere Between the Lines_, le premier single issu de leur premier album _More Or Less Forever_, entièrement auto-produit.

Le clip suit les pérégrinations de Lara, aussi bien dans dans Paris que dans son esprit. Vous reconnaîtrez probablement des lieux emblématiques du 13ème arrondissement de Paris, où Lara a longtemps habité.

Le single _Somewhere Between The Lines_ sortira sur les plateformes de streaming le 6 mai chez Verveine Records.

## Liens

- [Clip de Somewhere Between The Lines sur YouTube](https://www.youtube.com/watch?v=yyVhakmMVbY)
- [Kit presse d'Allflowers]({{< param "pressKitFr" >}})

## Photos

Cliquez sur les photos pour une version en haute résolution.

{{< image "somewhere-between-the-lines-thumbnail.jpg" "" "Photo de couverture" "yes" "medium" "" >}}

{{< image "somewhere-between-the-lines-single.jpg" "" "Pochette du single Somewhere Between The Lines" "yes" "medium" >}}

{{< image "somewhere-between-the-lines-snapshot-1.jpg" "" "Capture d'écran de la vidéo (1)" "yes" "medium" "" >}}

{{< image "somewhere-between-the-lines-snapshot-2.jpg" "" "Capture d'écran de la vidéo (2)" "yes" "medium" "" >}}
