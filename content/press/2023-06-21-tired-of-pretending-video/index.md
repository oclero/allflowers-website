---
title: Allflowers présentent le clip de leur nouveau single "Tired of Pretending"
date: 2023-06-21
---

Le duo de rock alternatif **Allflowers** a célébré le solstice 2023 avec la sortie du clip de _Tired of Pretending_, le nouveau single issu de leur premier album _More Or Less Forever_.

Le morceau se présente comme un journal intime dans lequel Lara exprime ses doutes et se confie, pour finalement se clôturer dans un tourbillon de guitares étourdissantes.

Le clip a été entièrement réalisé et produit par la réalisatrice **Nikita Blauwart**, une amie de longue date. Tout comme le clip de _Somewhere Between the Lines_, il a été tourné dans le quartier du 13e arrondissement de Paris. Si le premier clip était ensoleillé, celui-ci prend le contrepied et adopte un ton majoritairement plus sombre, utilisant des astuces visuelles pour déformer l’image.

Le single _Tired of Pretending_ sortira sur les plateformes de streaming le 22 juillet chez Verveine Records.

## Liens

- [Clip de Tired of Pretending sur YouTube](https://www.youtube.com/watch?v=cXBs700IBog)
- [Page de Nikita Blauwart sur Vimeo](https://vimeo.com/user12061048)
- [Kit presse d'Allflowers]({{< param "pressKitFr" >}})

## Photos

Cliquez sur les photos pour une version en haute résolution.

{{< image "tired-of-pretending-thumbnail.jpg" "" "Photo de couverture" "yes" "medium" "Nikita Blauwart" >}}

{{< image "tired-of-pretending-single.jpg" "" "Pochette du single Tired of Pretending" "yes" "medium" >}}

{{< image "allflowers-28.jpg" "" "Photo du groupe" "yes" "medium" "Judith Wahler" >}}

{{< image "tired-of-pretending-snapshot-1.jpg" "" "Capture d'écran de la vidéo (1)" "yes" "medium" "Nikita Blauwart" >}}

{{< image "tired-of-pretending-snapshot-2.jpg" "" "Capture d'écran de la vidéo (2)" "yes" "medium" "Nikita Blauwart" >}}
