---
title: Allflowers sortent leur single de Noël "Winter Letter"
date: 2023-12-20
---

Le duo de rock alternatif **Allflowers** a décidé de célébrer les fêtes de fin d’année comme il se doit en se pliant à l’exercice de la chanson de Noël avec _Winter Letter_, une composition originale dans la veine orchestrale de _Zagreb Song_. Les paroles font une nouvelle fois référence à la capitale de la Croatie, d’où est originaire Lara, la chanteuse.

Ils avaient seulement deux semaines devant eux pour accomplir ce défi. En résulte cette chanson qui tente de capturer l'atmosphère généreuse et bienveillante de Noël. Le son s'éloigne un peu du style de leur premier album, pour se parer d'atours folk grâce à la présence d'instruments traditionnels (joués par Cécile Passaquay et Magali Cadas) et de chœurs exubérants.

Le single _Winter Letter_ sort sur les plateformes de streaming le 20 décembre 2023 chez Verveine Records et se savoure comme un bon chocolat de Noël.

## Liens

- [Clip de Winter Letter sur YouTube](https://www.youtube.com/watch?v=5R9tmKER-W0)
- [Kit presse d'Allflowers]({{< param "pressKitFr" >}})

## Photos

Cliquez sur les photos pour une version en haute résolution.

{{< image "winter-letter-thumbnail.jpg" "" "Photo de couverture" "yes" "medium" "Vera Zahorodni, Olivier Cléro" >}}

{{< image "winter-letter-single.jpg" "" "Pochette du single Winter Letter" "yes" "medium" >}}

{{< image "winter-letter-snapshot-1.jpg" "" "Capture d'écran de la vidéo (1)" "yes" "medium" "Vera Zahorodni, Olivier Cléro" >}}

{{< image "winter-letter-snapshot-2.jpg" "" "Capture d'écran de la vidéo (2)" "yes" "medium" "Vera Zahorodni, Olivier Cléro" >}}

{{< image "winter-letter-snapshot-3.jpg" "" "Capture d'écran de la vidéo (3)" "yes" "medium" "Vera Zahorodni, Olivier Cléro" >}}

{{< image "winter-letter-snapshot-4.jpg" "" "Capture d'écran de la vidéo (4)" "yes" "medium" "Vera Zahorodni, Olivier Cléro" >}}

{{< image "winter-letter-snapshot-5.jpg" "" "Capture d'écran de la vidéo (5)" "yes" "medium" "Vera Zahorodni, Olivier Cléro" >}}
