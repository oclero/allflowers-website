---
title: Shout
trackNumber: 1
---

{{< youtube 8gKXLgf4wH4 >}}

Tears for Fears cover.

```txt
Shout, shout, let it all out
These are the things I can do without
Come on
I'm talking to you, come on

In violent times, you shouldn't have to sell your soul
In black and white, they really, really ought to know

Those one-track minds that took you for a working boy
Kiss them goodbye, you shouldn't have to jump for joy
You shouldn't have to jump for joy

They gave you life and in return, you gave them hell
As cold as ice, I hope we live to tell the tale
I hope we live to tell the tale

And when you've taken down your guard
If I could change your mind, I'd really love to break your heart
I'd really love to break your heart
```
