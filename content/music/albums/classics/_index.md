---
draft: true
title: Classics
date: 2023-01-04
type: lists
layout: album
cover: cover.jpg
label: Verveine Records
releaseType: LP
released: false
platforms:
  - name: bandcamp
    url: https://allflowers.bandcamp.com/album/classics
  - name: soundcloud
    url: https://soundcloud.com/allflowers/sets/classics
  - name: youtube
    url: https://www.youtube.com/watch?v=8gKXLgf4wH4&list=PL5Z4OiO-L5_8wOnCaymEn-t3PSKJPGpAH
---

## About

We decided to pay homage to some artists and songs we like by reworking their songs with different arrangements, mostly in a guitar+voice vein, with vocal harmonies, similar to Simon & Garfunkel's vibe.

We'll add tracks progressively, and release the album on streaming platforms when it's ready.

## Credits

- Vocals: [**Lara Martinovic**](https://www.instagram.com/lara.stuffs)
- Instruments, Arrangements, Production: [**Olivier Cléro**](https://www.instagram.com/olivier.clero/)
- Cover picture: [**Judith Wahler**](https://www.instagram.com/juwahnji)
