---
title: Emptiness
trackNumber: 1
---

Lara's very first composition (2017). She originally entirely arranged this song on a small Android app (Soundcamp) and it has been redone from scratch in 2019.

The synthetic string instruments were replaced by real ones, played by Catherine Jacob and Barbara Rialland, Olivier’s friends, and inspired by The Verve's _Bittersweet Symphony_, among others.

The introductory melody, which is the starting point of the song, was born long before the song’s composition and played in Lara's head since her teenage years.

```txt
[Verse]
(Dsus2)
Nothing to say
(Bm7 C G) or (D/B D/C D/B)
I've got no more to say
Nothing to say, please
Let me run away
Time's running out
I just woke up today
All these days passing by
And my mind stuck in here
And yet

[Chorus]
Here I am, here I am
Here I am
Wading through this crazy emptiness
Here I am

When you grow up
When you grow up one day
Then remember
Just do what I say
'Cause time's running out
There's not much time left for you
To do all the things that you're expected to do

And yet
Here I am, here I am
Here I am, wading through this crazy emptiness
Here I am
```
