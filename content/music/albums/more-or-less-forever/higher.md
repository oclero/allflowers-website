---
title: Higher
trackNumber: 5
---

This was originally an instrumental composition by Olivier (2019) to which Lara added her voice.

Higher is one of the rockier tracks on the album, influenced by Smashing Pumpkins first albums (_Today_, for instance) and Wolf Alice.

```txt
(D)
Oh, every day I feel different
(Fm7 G)
And every day is a new beginning
I don’t know how things will turn out
(Bm7 G)
But I can say

(D)
That we’re gonna go higher
(Bm7 G)
Than we ever hoped for
We’re gonna go higher
We won’t stick to the road
We’re gonna go higher
Than we ever hoped for
(B)
Higher

(E G B)
And in the blue light I see
Golden dots getting brighter
And in the noise I can hear
(A G)
That’s just the way it is
(F E)
…But that’s what they think

Here is the place
Where I can breathe
Where I can finally feel for real
No more questions
No more fears
It feels so lighter

And we’re gonna go higher
Than we ever hoped for
We’re gonna go higher
We won’t stick to the road
We’re gonna go higher
Than we ever hoped for
Higher

And through the blue veil, I see
Dots of light getting brighter
And in the noise I can hear
That’s just the way it is
…But that’s what they think

(F G)
Blue lights dancing
(D A)
I gets more and more clear
(F G)
Golden letters
(A)
The word you were waiting for
```
