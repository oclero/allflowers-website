---
title: Tired of Pretending
trackNumber: 8
---

{{< youtube cXBs700IBog >}}

This was originally an instrumental composition by Olivier (2016) that set a speech by Charlie Chaplin to music. It was gradually transformed to its final form. The choruses were found quickly, while the verses took longer to take shape.

A darkly atmospheric piece with spoken verses, sung choruses (inspired by Massive attack), and a post-rock instrumental finale inspired by Radiohead and the Beatles' _I Want You (She's So Heavy)_.

```txt
(C#m7 E/B F#add9/A# Amaj7)
See
I’m like the character of your book
Stuck in the same old situation
Day after day

And I’m trying and trying
And trying
But I’m always falling back to the same place

What should I say
Say
When I look into your eyes

(C#m7 C#m7/B F#add9/A# Amaj7)
I’m sorry it’s not worth talking
Stop it - I’ll tell you one thing
All this is not worth to me
And I’m tired of pretending

And the river is flowing
And the trees are blooming
But the sun doesn’t shine
As you would like it to

What Have I done
What have I done to you
I’m afraid
I pray

I’m sorry it’s not worth talking
Stop it - I’ll tell you one thing
All this is not worth to me
And I’m tired of pretending

I’m sorry it’s not worth talking
Stop it - I’ll tell you one thing
All this is not worth to me

(G#7 Eadd9)
Burning flame
Burning star
Don’t be eager, I’ll be your guide
I’ll be by your side
(A A/G# F#m C#m7)
And I’ll send you my light sparkles
(C#m7 E x2)
From my soul to yours

(C#m7 E7 A Am6)
From my soul to yours
```
