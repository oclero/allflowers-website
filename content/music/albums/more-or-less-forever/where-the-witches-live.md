---
title: Where the Witches Live
trackNumber: 9
---

This was originally a new wave style composition by Lara (2018), which went through many versions, including a metal/grunge (!) version, before being completely redone at the very last moment.

The finale is inspired by Fleetwood Mac's _The Chain_.

The song originally had only one verse and Olivier joined in writing the lyrics for the second verse.

```txt
(C#5 Amaj7 F#sus2 G#sus4)
In the back of my mind
In the back of your mind
At the end of the street
There, where the children play

In the church, in the park
In the hall of your block
In the crowd underground
There, where the witches live

(E5 C#5 A5 F#5)
There where the witches live
There where the witches live

(C# A E G#)
Come with us
Hide in the game
Come with us
Hide in the game
(C# D A G# G)
Would be fun to meet you there
Around the corner, who knows where
Would be fun to meet you there

Run away from the bell
Stay away from the spell
From the reach of their sight
So they won’t get us at night

For the shade in their eyes
And the mist in their hearts
Feed their lies
Silently
Never will they reach the city

There, Where the Witches live
There, Where the Witches live

(C#m C#m/B C#m/A# Cm#/A)
Oh ooh-ooh ooh oh-woo-ooh-ooh
Oh ooh-ooh ooh oh-woo-ooh

Come with us
Hide in the game
Come with us
Hide in the game
Would be fun to meet you there
Around the corner, who knows where
Would be fun to meet you there

(C#m A F#m G#add11)
In the church, in the park
In the hall of your block
In the crowd underground
There, where the witches live
(x2)

(C#m A F#m G#7)
There, where the witches live
```
