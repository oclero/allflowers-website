---
title: Thought I Told You
trackNumber: 10
---

Composition by Olivier (2019), who wanted a long progressive song to close the album.

The song is made of 4 parts, starting on a worrying tone, then leading to a clearing, before becoming again a little more tormented, then ending on a guitar solo in harmonies.

The Verve's guitar tracks inspired the rocking parts of this song.

```txt
(F#m Eadd9 Bm D6)
Thought I told you
What the thing was all about
Tried my best to
Get the picture in your eyes

What was out there
I tried hard to hold it back
I’d like to go back
To the start

What was out there
I tried hard to…
Ooh…

Can you tell me
Why you are so sure about
What is here now
What is right, what is a lie

(E Bmadd11 Dsus2 Asus2)
What was the rubbish you just said
Please repeat, I didn’t get you well
Why won’t you admit
I was living in so much pain

This is how it all began, you said
You led me to that point in such a way
You set me up
Ooh ooh-ooh-ooh ooh-ooh
You set me up
Did you set me up?
Did you set me up?

(F#sus2 Esus2 Bm7-Bm6 D6-D(b5))
I thought I told you why
The tears came to my eyes
Maybe you were right
How could I figure out
What it was all about
I was waiting for a light

Ooh
I’d like to fly
To Where the sun is shining bright

You set me up
(E Bmadd11 Dsus2 Asus2)
(E Dsus2)
```
