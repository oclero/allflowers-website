---
title: Hey! This Way
trackNumber: 4
---

{{< youtube EYG3IJCXtrQ >}}

Composition by Lara on the theme of renewal.

The song has an original structure, which does not alternate verses and choruses, but is composed of several all distinct parts.

The fast tempo final instrumental is a distant nod to Lynyrd Skynyrd's _Free Bird_ and the melody of Joy Division's _Love Will Tear Us Apart_.

```txt
(F)
Walking down the street
(Ebadd9)
Sunny day
(Ab6)
Looking ‘round, looking up
(F)
To the Sky
I’m thinking I won’t look back
To the clouds
Far behind it’s all over
Over!

I feel free like the bird
In the air
I see it land on the tree
In bright sunlight
And it is looking at me
And I feel clearly
It is saying
Hey! This Way!
This Way!

Your life is calling out for you
And I hear clearly it shouting
Hey! This Way, This Way
(x2)

(F)
The door opened (yeah)
(Ab)
On a new dimension
(Bb)
Another direction
(F)
A new destination
(x2)

A new destination!
```
