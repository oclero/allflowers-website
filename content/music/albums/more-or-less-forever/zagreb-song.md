---
title: Zagreb Song
trackNumber: 2
---

{{< youtube w2PK0eooleM >}}

Composition by Lara (2018) with the particularity of not having a chorus.

The ballad was inspired by Lara's travels in her childhood to Croatia, her family's homeland, and composed in Zagreb during the summer of 2018.

The song took time to find its final form: it went through several different styles before revealing itself in an acoustic pop-folk form.

The instrumental finale is inspired by _Lucky Man_ by The Verve.

```txt
(Asus2)
Driving for much too long
Driving for much too long
(F#m7add11)
Feeling heavy and painful now

(Asus2)
I try to open my eyes to see
(Asus2/G#)
I open my eyes to see
(B7add11)
Passing by clouds and trees
Familiar streets and buildings

Empty parks, empty streets
In this summertime atmosphere
Feel the quiet all around
Everything is peaceful all around

Now I put my feet on the ground
Take a breath deep inside
Feel the difference in the air
Can you feel the difference in the air

I put my feet on the ground
Take a breath deep inside
Feel the difference in the air
Can you feel the difference in the air
(x2)

Time has passed
The feeling's gone
It turned into
A weak memory
A shadow
A blurred shadow
(x2)

In the air
In the air
```
