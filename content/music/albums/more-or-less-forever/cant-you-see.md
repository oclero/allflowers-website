---
title: Can't You See
trackNumber: 7
---

Vocal part originally composed by Lara (2018), on a Dark Synthwave style instrumental for an unfinished project.

The melody fit well with a composition by Olivier (2016), and the two were merged.

This gives in the end a rather dark rock track, fast and led by a violin and cello riff. The drums are inspired by _Tonight, Tonight_ by the Smashing Pumpkins.

```txt
(C#m)
Time flies
When they’re in your mind
Always the same
When you would give it a try
(Amaj7 F#m9)
Can’t you see

(G#7 C#sus2 Esus2)
Oh-ooh-oh

Time flies
With them all around
Again and again
When you try to get out
Can’t you see

Oh-ooh-oh
Now you see…

Time flies
When they’re in your mind
Always the same
When you would give it a try
Can’t you see

Time flies
With them all around
Again and again
Now why won’t you get out
Can’t you see

Can’t you see
Can’t you see

(C#sus2 Asus2 Esus2 Dsus2)
Ooh

Now you see
Can’t you see
```
