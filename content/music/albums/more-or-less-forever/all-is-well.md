---
title: All Is Well
trackNumber: 11
---

Instrumental composition by Olivier (2020), inspired by The Beatles' _Dear Prudence_ and Mazzy Star's _Into Dust_. The only totally acoustic track of the album. The calm after the storm.

The lyrics came naturally to Lara and it was the fastest song to write, along with the first one on the album.

Despite its apparent softness and simplicity, the track leads to a stranger atmosphere and ends in a bath of distortion and reverb, disturbing or mystical according to one’s perceptions.

```txt
(Bm7 A#maj7 D)
See the softness of the clouds
Slowly drifting across the sky

There’s nothing to do
Nothing to think about
Nothing to prove
No one to satisfy

(Gmaj7 Gmadd9/A#)
All is well
(D A7sus4)
All is right
Don’t try to do
Don’t try to fight

All is well
All is right
Don’t try to do
Don’t try to fight

There’s nothing to say
Nothing to play
Nothing to hide
No need to explain

There’s nothing to find
Nothing to wait for
There’s nothing to doubt
No need to fear
No more

All is well
All is right
Don’t try to do
Don’t try to fight
```
