---
title: Somewhere Between the Lines
date: 2022-05-06
type: lists
layout: album
cover: cover.jpg
label: Verveine Records
labelUrl: /verveine-records
releaseType: single
released: true
platforms:
  - name: spotify
    url: https://open.spotify.com/album/7ESxyxJlxHcGwCvPZSKXE8
  - name: deezer
    url: https://www.deezer.com/album/458629565
  - name: appleMusic
    url: https://music.apple.com/album/somewhere-between-the-lines-single/1694728248
  - name: bandcamp
    url: https://allflowers.bandcamp.com/album/somewhere-between-the-lines
  - name: youtube
    url: https://www.youtube.com/watch?v=yyVhakmMVbY
tracklist:
  - title: Somewhere Between the Lines
    url: /music/albums/more-or-less-forever/somewhere-between-the-lines
---

## Credits

Composed, written and arranged by **Lara Martinovic** and **Olivier Cléro**, and published by **Verveine Records**. Recorded, mixed and mastered in Paris, France, at **Verveine Studios** between November 2018 and November 2021.

- **Lara Martinovic:** Vocals.
- **Olivier Cléro:** Guitar, Bass, Drums.

Picture credits:

- **Olivier Cléro:** Cover picture.
