---
title: Zagreb Song
date: 2023-10-28
type: lists
layout: album
cover: cover.jpg
label: Verveine Records
labelUrl: /verveine-records
releaseType: single
released: true
platforms:
  - name: spotify
    url: https://open.spotify.com/album/4dszdetP0y0typKZMAknXL
  - name: deezer
    url: https://www.deezer.com/album/501051281
  - name: appleMusic
    url: https://music.apple.com/album/zagreb-song-single/1712275406
  - name: bandcamp
    url: https://allflowers.bandcamp.com/album/zagreb-song
  - name: youtube
    url: https://www.youtube.com/watch?v=w2PK0eooleM
tracklist:
  - title: Zagreb Song
    url: /music/albums/more-or-less-forever/zagreb-song
---

## Credits

Composed, written and arranged by **Lara Martinovic** and **Olivier Cléro**, and published by **Verveine Records**. Recorded, mixed and mastered in Paris, France, at **Verveine Studios** between November 2018 and November 2021.

- **Lara Martinovic:** Vocals, Clarinet.
- **Olivier Cléro:** Guitars, Bass, Drums.
- **Louis Couka:** Fender Rhodes.
- **Catherine Jacob:** Violins.
- **Barbara Rialland:** Cello.

Picture credits:

- **Olivier Cléro:** Cover picture.
