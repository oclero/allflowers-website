---
title: Winter Letter
trackNumber: 1
---

{{< youtube 5R9tmKER-W0 >}}

Writing the song was a challenge: what if we created our own Christmas song?

We had two weeks ahead of us to make it, and wanted to avoid the usual cheesiness of this kind of song, so we worked hard, and here it is! We've tried to capture the generous and benevolent atmosphere of Christmas, with traditional instruments, impressionistic lyrics and exuberant choruses. We hope you enjoy it!

```txt
(Asus2 Esus4 D)
Honey, hi
Sweet hedgehog of mine
Greetings from here
From the deep blue night

(Bm7 D Bm7 E)
I’ll send you soon a letter
With Zagreb snow inside

(Asus2 Esus4 D)
Brewing joy
In cinnamon light
See, don’t be mad
Come to me by my side

(Bm7 D Bm7 E)
I’ll send you soon a letter
With your favourite song inside
```
