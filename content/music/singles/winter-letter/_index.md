---
title: Winter Letter
date: 2023-12-20
type: lists
layout: album
cover: cover.jpg
label: Verveine Records
labelUrl: /verveine-records
releaseType: single
released: true
platforms:
  - name: spotify
    url: https://open.spotify.com/album/30yhlARAbNw7iWyRJaLcOs
  - name: deezer
    url: https://www.deezer.com/us/album/520299542
  - name: appleMusic
    url: https://music.apple.com/album/winter-letter-single/1719804787
  - name: bandcamp
    url: https://allflowers.bandcamp.com/album/winter-letter
  - name: youtube
    url: https://www.youtube.com/watch?v=5R9tmKER-W0
  - name: soundcloud
    url: https://soundcloud.com/allflowers/winter-letter
tracklist:
  - title: Winter Letter
    url: /music/singles/winter-letter/winter-letter
---

## Music Video

{{< youtube 5R9tmKER-W0 >}}

## Credits

Composed, written and arranged by **Lara Martinovic** and **Olivier Cléro**, and published by **Verveine Records**. Recorded, mixed and mastered in Paris, France, at **Verveine Studios** in November 2023.

- **Lara Martinovic:** Vocals.
- **Olivier Cléro:** Guitars, Mandolin, Bass, Drums.
- **Cécile Passaquay:** Violin.
- **Magali Cadas:** Cello.

Picture credits:

- **Olivier Cléro:** Cover picture.
