---
title: Hey! This Way
date: 2024-03-02
type: lists
layout: album
cover: cover.jpg
label: Verveine Records
labelUrl: /verveine-records
releaseType: single
released: true
platforms:
  - name: spotify
    url: https://open.spotify.com/album/0x8SqCD008s3bL84VLlMU3
  - name: deezer
    url: https://www.deezer.com/us/album/545359752
  - name: appleMusic
    url: https://music.apple.com/fr/album/hey-this-way-single/1729832246
  - name: bandcamp
    url: https://allflowers.bandcamp.com/album/hey-this-way
  - name: youtube
    url: https://www.youtube.com/watch?v=EYG3IJCXtrQ
tracklist:
  - title: Hey! This Way
    url: /music/albums/more-or-less-forever/hey-this-way
---

## Music Video

{{< youtube EYG3IJCXtrQ >}}

## Credits

Composed, written and arranged by **Lara Martinovic** and **Olivier Cléro**, and published by **Verveine Records**. Recorded, mixed and mastered in Paris, France, at **Verveine Studios** between November 2018 and November 2021.

- **Lara Martinovic:** Vocals, Piano.
- **Olivier Cléro:** Guitar, Bass, Drums.

Picture credits:

- **Frenshyte Prod:** Cover picture.
- **Olivier Cléro:** Cover design.
